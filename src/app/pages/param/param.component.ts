import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-param',
  templateUrl: './param.component.html',
  styleUrls: ['./param.component.scss']
})
export class ParamComponent implements OnInit {

  public id: number;
  public id2: number;
  public name: string;
  public name2: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = Number(params.get("id"));
      this.name = params.get("name");
    });

    this.id2 = this.route.snapshot.params.id;
    this.name2 = this.route.snapshot.params.name;
  }

}
