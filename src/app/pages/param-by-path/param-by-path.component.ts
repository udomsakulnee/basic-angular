import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-param-by-path',
  templateUrl: './param-by-path.component.html',
  styleUrls: ['./param-by-path.component.scss']
})
export class ParamByPathComponent implements OnInit {

  public name: string;
  public name2: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.name = params.get("name");
    });

    this.name2 = this.route.snapshot.params.name;
  }

}
