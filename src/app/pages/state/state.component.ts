import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.scss']
})
export class StateComponent implements OnInit {

  public id: number;
  public name: string;

  constructor(private router: Router) {
    this.id = this.router.getCurrentNavigation().extras.state.id;
    this.name = this.router.getCurrentNavigation().extras.state.name;
  }

  ngOnInit() {
  }

}
