import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public sendParamByPath(): void {
    this.router.navigate(['/pages/param-by-path', 'marry']);
  }

  public sendParam(): void {
    this.router.navigate(['/pages/param', { id: 2, name: 'Bueng Kan' }]);
  }

  public sendQueryString(): void {
    this.router.navigate(['/pages/query-string'], { queryParams: { id: 4, name: 'Khon Kaen' } });
  }

  public sendState(): void {
    this.router.navigateByUrl('/pages/state', { state: { id: 5, name: 'Phuket' } });
  }
}
