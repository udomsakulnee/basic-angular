import { PagesComponent } from './pages.component';
import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: "",
        component: PagesComponent,
        children: [
            {
                path: "",
                redirectTo: "navigation",
                pathMatch: "prefix"
            },
            {
                path: "navigation",
                loadChildren: () => import("./navigation/navigation.module").then(m => m.NavigationModule)
            },
            {
                path: "param-by-path/:name",
                loadChildren: () => import("./param-by-path/param-by-path.module").then(m => m.ParamByPathModule)
            },
            {
                path: "param",
                loadChildren: () => import("./param/param.module").then(m => m.ParamModule)
            },
            {
                path: "query-string",
                loadChildren: () => import("./query-string/query-string.module").then(m => m.QueryStringModule)
            },
            {
                path: "state",
                loadChildren: () => import("./state/state.module").then(m => m.StateModule)
            }
        ]
    }
]