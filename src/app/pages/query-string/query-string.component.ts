import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-query-string',
  templateUrl: './query-string.component.html',
  styleUrls: ['./query-string.component.scss']
})
export class QueryStringComponent implements OnInit {

  public id: number;
  public name: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParamMap.subscribe((params: ParamMap) => {
      this.id = Number(params.get("id"));
      this.name = params.get("name");
    });
  }

}
