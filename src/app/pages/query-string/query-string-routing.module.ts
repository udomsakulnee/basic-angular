import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QueryStringComponent } from './query-string.component';


const routes: Routes = [
  {
    path: "",
    component: QueryStringComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QueryStringRoutingModule { }
